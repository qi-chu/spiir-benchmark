all: table.pdf

table.pdf: table.tex
	latexmk -xelatex table.tex

clean:
	latexmk -c
